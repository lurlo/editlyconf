# Package

version       = "1.1.3"
author        = "Luciano Lorenzo"
description   = "Editly config generation tools and types"
license       = "gpl3-only"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.4"
